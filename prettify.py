PUNCT = set('.?,;!:')


def remove_spaces(text):
    result = []
    for i, s in enumerate(text.split()):
        if s[0] not in PUNCT and len(result) > 0:
            result.append(' ')
        result.append(s)
    return ''.join(result)


def postprocess(text):
    text = text.replace('<CENSORED>', '***')  # don't confuse it with html tags
    text = remove_spaces(text)
    return text
