import pandas as pd
import pickle

from sklearn.pipeline import make_pipeline, make_union
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.decomposition import TruncatedSVD
from sklearn.naive_bayes import BernoulliNB
from sklearn.preprocessing import Normalizer

from sampler import NeigborSampler, NoiseAdder


def make_sampler(words_dim=300, chars_dim=50, max_char_ngram=4, min_df_word=3, min_df_char=3):
    return make_pipeline(
        make_union(
            make_pipeline(
                TfidfVectorizer(min_df=min_df_word, token_pattern=r"(?u)\b\w+\b|!|\?"),
                TruncatedSVD(n_components=words_dim),
                Normalizer(),
            ),
            make_pipeline(
                TfidfVectorizer(min_df=min_df_char, analyzer='char_wb', ngram_range=(1, max_char_ngram)),
                TruncatedSVD(n_components=chars_dim),
                Normalizer(),
            ),
        ),
        NoiseAdder(eps=0.001),
        Normalizer(),
        NeigborSampler(),
    )


def train_english():
    english_data = pd.read_csv('english_data.tsv', sep='\t', encoding='utf-8')
    pipe_en = make_sampler(
        min_df_word=5,
        min_df_char=5,
        max_char_ngram=4,
        words_dim=100,
        chars_dim=100
    ).fit(english_data['q'], english_data['r'])

    # we want the index to be small, but SVD should be trained on the whole sample
    smaller_part = english_data.sample(frac=0.1, random_state=42)
    body = make_pipeline(*[step[1] for step in pipe_en.steps[:-1]])
    smaller_trf = body.transform(smaller_part.q)
    pipe_en.steps[-1][1].fit(smaller_trf, smaller_part.r)

    with open('chatbot_pipe_en.pkl', 'wb') as f:
        pickle.dump(pipe_en, f)


def train_russian():
    russian_data = pd.read_csv('russian_data.tsv', sep='\t', encoding='utf-8')
    pipe_ru = make_sampler().fit(russian_data['q'], russian_data['r'])
    with open('chatbot_pipe_ru.pkl', 'wb') as f:
        pickle.dump(pipe_ru, f)


def train_lang_detector():
    russian_data = pd.read_csv('russian_data.tsv', sep='\t', encoding='utf-8')
    english_data = pd.read_csv('english_data.tsv', sep='\t', encoding='utf-8').sample(
        int(russian_data.shape[0]*0.5), random_state=42)
    data_for_clf = pd.concat([
        pd.DataFrame({'text': russian_data.q, 'language': 'ru'}),
        pd.DataFrame({'text': english_data.q, 'language': 'en'})
    ])
    lang_detector = make_pipeline(
        CountVectorizer(analyzer='char', ngram_range=(1, 2), min_df=10),
        BernoulliNB(alpha=10)
    ).fit(data_for_clf.text, data_for_clf.language)
    with open('lang_detector.pkl', 'wb') as f:
        pickle.dump(lang_detector, f)


if __name__ == '__main__':
    train_english()
    train_russian()
    train_lang_detector()
