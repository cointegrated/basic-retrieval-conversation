
import numpy as np
from sklearn.base import BaseEstimator
from sklearn.neighbors import BallTree


def softmax(x):
    proba = np.exp(-x)
    return proba / sum(proba)


class NeigborSampler(BaseEstimator):
    def __init__(self, k=5, temperature=1.0):
        self.k = k
        self.temperature = temperature

    def fit(self, X, y):
        self.tree_ = BallTree(X)
        self.y_ = np.array(y)

    def predict(self, X, random_state=None):
        distances, indices = self.tree_.query(X, return_distance=True, k=self.k)
        result = []
        for distance, index in zip(distances, indices):
            result.append(np.random.choice(index, p=softmax(distance * self.temperature)))
        return self.y_[result]

    def transform(self, X):
        distances, indices = self.tree_.query(X, return_distance=True, k=self.k)
        return indices


class NoiseAdder(BaseEstimator):
    """ Just add spherical noise! """
    def __init__(self, eps=0.001):
        self.eps = eps

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        return X + np.random.normal(scale=self.eps, size=X.shape)
