import os
import pickle

from flask import Flask, request, jsonify, abort

from prettify import postprocess


with open('chatbot_pipe_en.pkl', 'rb') as f:
    pipe_en = pickle.load(f)
with open('chatbot_pipe_ru.pkl', 'rb') as f:
    pipe_ru = pickle.load(f)
with open('lang_detector.pkl', 'rb') as f:
    lang_detector = pickle.load(f)

lang2gc = {
    'ru': pipe_ru,
    'en': pipe_en
}

BASE_URL = 'https://boltalka-as-a-service.herokuapp.com/'

server = Flask(__name__)


def respond(utterance):
    lang = lang_detector.predict([utterance])[0]
    response = lang2gc[lang].predict([utterance])[0]
    return response


@server.route("/boltalka_api",  methods=['POST'])
def boltalka_api():
    if not request.json:
        abort(400)
    if 'utterance' not in request.json:
        abort(400)
    text = respond(request.json.get('utterance', ''))
    final_text = postprocess(text)
    return jsonify({'response': final_text}), 201


if __name__ == "__main__":
    server.run(host="0.0.0.0", port=int(os.environ.get('PORT', 5000)))
